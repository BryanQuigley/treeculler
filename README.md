# treeculler
Implements frustum culling. In the future, it will also implement a few occlusion culling methods, and a BVH (bounding volume hierarchy) based frustum culling.