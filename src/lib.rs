//! Utilities to use for frustum culling. Comes with AABB and Sphere culling.
//!
//! Re-exports types and traits needed for implementing `BVol`s.

// Implemented thanks to these posts:
// - https://fgiesen.wordpress.com/2010/10/17/view-frustum-culling/
// - http://old.cescg.org/CESCG-2002/DSykoraJJelinek/
// - https://iquilezles.org/www/articles/frustumcorrect/frustumcorrect.htm

/// Bounding volumes. Includes the trait, implementations for AABB and Sphere.
///
/// Bounding volume implementations contain testing against plane logic.
/// Implementations may choose to implement derived trait functions if
/// optimizations for a volume can be done.
pub mod bvol;
/// A view frustum.
pub mod frustum;

pub use bvol::{BVol, BoundingSphere, AABB};
pub use frustum::Frustum;
pub use num_traits::Float;
pub use vek::vec::{Vec3, Vec4};

use std::fmt::Debug;

/// Calculates distance between plane and point
pub fn dist_bpp<T: Float>(plane: &Vec4<T>, point: Vec3<T>) -> T {
    plane.x * point.x + plane.y * point.y + plane.z * point.z + plane.w
}

/// Calculates the most inside vertex of an AABB.
pub fn mi_vertex<T: Float + Debug>(plane: &Vec4<T>, aabb: &AABB<T>) -> Vec3<T> {
    Vec3::new(
        if plane.x >= T::zero() {
            aabb.max.x
        } else {
            aabb.min.x
        },
        if plane.y >= T::zero() {
            aabb.max.y
        } else {
            aabb.min.y
        },
        if plane.z >= T::zero() {
            aabb.max.z
        } else {
            aabb.min.z
        },
    )
}

/// Calculates the most outside vertex of an AABB.
pub fn mo_vertex<T: Float + Debug>(plane: &Vec4<T>, aabb: &AABB<T>) -> Vec3<T> {
    Vec3::new(
        if plane.x >= T::zero() {
            aabb.min.x
        } else {
            aabb.max.x
        },
        if plane.y >= T::zero() {
            aabb.min.y
        } else {
            aabb.max.y
        },
        if plane.z >= T::zero() {
            aabb.min.z
        } else {
            aabb.max.z
        },
    )
}
